/*
    SOBEL EDGE DETECTION for 256x256 .PGM files
    By Ireneusz Wieczorek
*/
#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <fstream>
#include <string>
#include <string.h>

using namespace std;

void rac(int *r, int *c, string tab){
    char *cstr, *p;
    int row,col;
    cstr = new char [tab.size()+1];
    strcpy(cstr,tab.c_str());
    p=strtok(cstr," ");
        sscanf(p,"%d",&row);
        *c=row;
    p=strtok(NULL,"\n");
        sscanf(p,"%d",&col);
        *r=col;
}

void rotation(int operational[3][3], int selected[3][3], int a){
    int i,j;
    for(i=0;i<3;i++){
        for(j=0;j<3;j++){
            operational[i][j]=(a)*selected[i][j];
        }
    }
}

//SOBEL EDGE DETECTION
void sobel(int r, int c, int **in, int **out, int rot){
//(rows, columns, in-put, out-put, rotation)
    int i,j,k;
    int S0[3][3]  ={{-1,0,1},{-2,0,2},{-1,0,1}},
        S45[3][3] ={{0,1,2},{-1,0,1},{-2,-1,0}},
        S90[3][3] ={{1,2,1},{0,0,0},{-1,-2,-1}},
        S135[3][3]={{2,1,0},{1,0,-1},{0,-1,-2}},
        S[3][3];
    if(rot == 0)   rotation(S,S0,1);
    if(rot == 45)  rotation(S,S45,1);
    if(rot == 90)  rotation(S,S90,1);
    if(rot == 135) rotation(S,S135,1);
    if(rot == 180) rotation(S,S0,-1);
    if(rot == 225) rotation(S,S45,-1);
    if(rot == 270) rotation(S,S90,-1);
    if(rot == 315) rotation(S,S135,-1);
    for(i=1;i<r-1;i++){
        for(j=1;j<c-1;j++){
            out[i][j]=in[i-1][j-1]*(S[0][0]) + in[i-1][j]*(S[0][1]) + in[i-1][j+1]*(S[0][2])+
                      in[i][j-1] * (S[1][0]) + in[i][j] * (S[1][1]) + in[i][j+1] * (S[1][2])+
                      in[i+1][j-1]*(S[2][0]) + in[i+1][j]*(S[2][1]) + in[i+1][j+1]*(S[2][2]);
            if(out[i][j]<0) out[i][j]=0;
            if(out[i][j]>255) out[i][j]=255;
        }
    }
    for(i=1;i<c-1;i++){
        out[0][i]=out[1][i];
        out[r-1][i]=out[r-2][i];
    }
    for(i=0;i<r;i++){
        out[i][0]=out[i][1];
        out[i][c-1]=out[i][c-2];
    }
}
//BY IRENEUSZ WIECZOREK

int main(){
    int i,j;
    int rows,columns;
//STRT- ARRAY DECLARATION
    int wi=4000, ko=4000;
    int **f_working_v = new int *[wi];
    for(i=0;i<wi;i++)
        f_working_v[i] = new int [ko];
    int **f_output_v = new int *[wi];
    for(i=0;i<wi;i++)
        f_output_v[i] = new int [ko];
//END - ARRAY DECLARATION
    string f_header[5],temp;
    char name[200];
    fstream f_original;
//READING FROM FILE
    cout<<"Set the path to file or insert image(.pgm)"<<endl;
    cout<<"in program directory and enter the name:"<<endl;
    cin>>name;
    f_original.open(name, ios::in);
    if(f_original.is_open() == 1){
        for(i=0;i<4;i++){
            getline(f_original,f_header[i]);
        }
        rac(&rows,&columns,f_header[2]);
        cout<<"image size: "<<rows<<"x"<<columns<<endl;
        for(i=0;i<rows;i++){
            for(j=0;j<columns;j++){
                getline(f_original,temp);
                sscanf(temp.c_str(), "%d", &f_working_v[i][j]);
            }
        }
    }
    else{
        cout<<"Error! Cannot open the file!"<<endl;
        exit(0);
    }
    f_original.close();
//SAVEING TO FILE
    fstream output;
    int rot_i,tests,t;
    cout<<"How many tests do you want to run?"<<endl;
    cin>>tests;
    if(tests>0)
        cout<<"Selecte angle  of rotation (0,45,90,135,180,225,270,315)"<<endl;
    for(t=0;t<tests;t++){
        char rot_s[3],rot_sn[30];
        cin>>rot_s;
        cout<<"set name:"<<endl;
        cin>>rot_sn;
        sscanf(rot_s,"%d",&rot_i);
        strcat(rot_sn,rot_s);
        strcat(rot_sn,".pgm");
        cout<<"Out-put file name: "<<rot_sn<<endl;
        sobel(rows, columns, f_working_v, f_output_v, rot_i);
        output.open(rot_sn, ios::out);
        cout<<"Please wait, computing.";
        for(i=0;i<4;i++){
            output<<f_header[i]<<endl;
        }
        cout<<".";
        for(i=0;i<rows;i++){
            for(j=0;j<columns;j++){
                output<<f_output_v[i][j]<<endl;
            }
        }
        cout<<"."<<endl;
        output.close();
        cout<<"Computing completed successfully"<<endl;
    }
    return 0;
}
